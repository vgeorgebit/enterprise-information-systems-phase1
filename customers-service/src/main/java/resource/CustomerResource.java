package resource;

import dao.CustomerDAO;
import model.Customer;
import model.ErrorMessage;
import org.jooby.Jooby;
import org.jooby.MediaType;
import org.jooby.Status;

public class CustomerResource extends Jooby {

	public CustomerResource(CustomerDAO dao) {

		path("/api/customers/customer", () -> {

			use("/:email", (req, rsp, chain) -> {

				String email = req.param("email").value();

				if (!dao.exists(email)) {
					rsp.status(Status.NOT_FOUND).send(new ErrorMessage("There is no customer with that username"));
				} else {
					chain.next(req, rsp);
				}

			});

			put("/:email", (req, rsp) -> {

				String email = req.param("email").value();

				Customer customer = req.body().to(Customer.class);

				if (!email.equals(customer.getEmail())) {
					rsp.status(Status.CONFLICT).send(new ErrorMessage("Email address can not be modified via this operation."));
				} else {
					dao.update(email, customer);
					rsp.send(customer);
				}
			});

			delete("/:email", (req, rsp) -> {
				String email = req.param("email").value();
				dao.delete(email);
				rsp.status(Status.NO_CONTENT);
			});

		}).produces(MediaType.json).consumes(MediaType.json);
	}
}
