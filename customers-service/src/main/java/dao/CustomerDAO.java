package dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import model.Customer;

public class CustomerDAO {

	private static final Map<String, Customer> customers = new TreeMap<>();

	public List<Customer> getAll() {
		return new ArrayList<>(customers.values());
	}

	public void save(Customer customer) {
		customers.put(customer.getEmail(), customer);
	}

	public Customer get(String emailAddress) {
		return customers.get(emailAddress);
	}

	public void delete(String emailAddress) {
		customers.remove(emailAddress);
	}

	public void update(String emailAddress, Customer updatedCustomer) {
		customers.put(emailAddress, updatedCustomer);
	}

	public boolean exists(String emailAddress) {
		return customers.containsKey(emailAddress);
	}

}
