package dao;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import model.Sale;
import model.Summary;

public class SaleDAO {

	private static final Multimap<String, Sale> salesByCustomer = ArrayListMultimap.create();
	private static final Map<String, Sale> salesBySaleId = new HashMap<>();

	private static final Double THRESHHOLD = 5000.0;

	static {

		if (salesByCustomer.isEmpty()) {
			Gson gson = new Gson();

			// create sale using an actual Vend sale so that we know we are compatible with Vend
			String json = "{\n"
			   + "  \"id\": \"fa7d04ba-c21e-855e-11e8-25c0e8cd045e\",\n"
			   + "  \"source\": \"USER\",\n"
			   + "  \"source_id\": null,\n"
			   + "  \"sale_date\": \"2018-03-12T06:45:44Z\",\n"
			   + "  \"status\": \"CLOSED\",\n"
			   + "  \"user_id\": \"0afa8de1-147c-11e8-edec-104f6535a398\",\n"
			   + "  \"customer_id\": \"0afa8de1-147c-11e8-edec-25c09e4a6f05\",\n"
			   + "  \"register_id\": \"0afa8de1-1450-11e8-edec-056e6ecf4cd9\",\n"
			   + "  \"market_id\": \"1\",\n"
			   + "  \"invoice_number\": \"1\",\n"
			   + "  \"short_code\": \"enjukv\",\n"
			   + "  \"totals\": {\n"
			   + "    \"total_price\": \"295.47826\",\n"
			   + "    \"total_loyalty\": \"0.00000\",\n"
			   + "    \"total_tax\": \"44.32174\",\n"
			   + "    \"total_payment\": \"339.80000\",\n"
			   + "    \"total_to_pay\": \"0.00000\"\n"
			   + "  },\n"
			   + "  \"note\": \"\",\n"
			   + "  \"updated_at\": \"2018-03-12T06:45:59+00:00\",\n"
			   + "  \"created_at\": \"2018-03-12 06:45:59\",\n"
			   + "  \"customer\": {\n"
			   + "    \"id\": \"0afa8de1-147c-11e8-edec-25c09e4a6f05\",\n"
			   + "    \"customer_code\": \"Doris-9CR9\",\n"
			   + "    \"customer_group_id\": \"0afa8de1-1450-11e8-edec-056e6ec340ed\",\n"
			   + "    \"first_name\": \"Doris\",\n"
			   + "    \"last_name\": \"Dolores\",\n"
			   + "    \"company_name\": \"\",\n"
			   + "    \"do_not_email\": false,\n"
			   + "    \"email\": \"doris@example.net\",\n"
			   + "    \"phone\": \"\",\n"
			   + "    \"mobile\": \"\",\n"
			   + "    \"fax\": \"\",\n"
			   + "    \"balance\": \"0.000\",\n"
			   + "    \"loyalty_balance\": \"0.00000\",\n"
			   + "    \"enable_loyalty\": true,\n"
			   + "    \"points\": 0,\n"
			   + "    \"note\": \"\",\n"
			   + "    \"year_to_date\": \"0.00000\",\n"
			   + "    \"sex\": \"F\",\n"
			   + "    \"date_of_birth\": null,\n"
			   + "    \"custom_field_1\": \"\",\n"
			   + "    \"custom_field_2\": \"\",\n"
			   + "    \"custom_field_3\": \"\",\n"
			   + "    \"custom_field_4\": \"\",\n"
			   + "    \"updated_at\": \"2018-03-12 06:43:05\",\n"
			   + "    \"created_at\": \"2018-03-12 06:43:05\",\n"
			   + "    \"deleted_at\": null,\n"
			   + "    \"contact_first_name\": \"Doris\",\n"
			   + "    \"contact_last_name\": \"Dolores\"\n"
			   + "  },\n"
			   + "  \"user\": {\n"
			   + "    \"id\": \"0afa8de1-147c-11e8-edec-104f6535a398\",\n"
			   + "    \"name\": \"mgeorge\",\n"
			   + "    \"display_name\": \"Mark George\",\n"
			   + "    \"email\": \"mark.george@otago.ac.nz\",\n"
			   + "    \"outlet_id\": null,\n"
			   + "    \"target_daily\": null,\n"
			   + "    \"target_weekly\": null,\n"
			   + "    \"target_monthly\": null,\n"
			   + "    \"created_at\": \"2018-02-12 23:49:42\",\n"
			   + "    \"updated_at\": \"2018-02-13 03:50:57\"\n"
			   + "  },\n"
			   + "  \"register_sale_products\": [\n"
			   + "    {\n"
			   + "      \"id\": \"fa7d04ba-c21e-855e-11e8-25c0eb708611\",\n"
			   + "      \"product_id\": \"0afa8de1-147c-11e8-edec-056e701f4190\",\n"
			   + "      \"quantity\": 1,\n"
			   + "      \"price\": \"60.78261\",\n"
			   + "      \"discount\": \"0.00000\",\n"
			   + "      \"loyalty_value\": \"0.00000\",\n"
			   + "      \"price_set\": false,\n"
			   + "      \"promotions\": {},\n"
			   + "      \"tax\": \"9.11739\",\n"
			   + "      \"price_total\": \"60.78261\",\n"
			   + "      \"tax_total\": \"9.11739\",\n"
			   + "      \"tax_id\": \"0afa8de1-1450-11e8-edec-056e6ec70277\"\n"
			   + "    },\n"
			   + "    {\n"
			   + "      \"id\": \"fa7d04ba-c21e-855e-11e8-25c0eed7f405\",\n"
			   + "      \"product_id\": \"0afa8de1-147c-11e8-edec-056e6f5a732f\",\n"
			   + "      \"quantity\": 1,\n"
			   + "      \"price\": \"234.69565\",\n"
			   + "      \"discount\": \"0.00000\",\n"
			   + "      \"loyalty_value\": \"0.00000\",\n"
			   + "      \"price_set\": false,\n"
			   + "      \"promotions\": {},\n"
			   + "      \"tax\": \"35.20435\",\n"
			   + "      \"price_total\": \"234.69565\",\n"
			   + "      \"tax_total\": \"35.20435\",\n"
			   + "      \"tax_id\": \"0afa8de1-1450-11e8-edec-056e6ec70277\"\n"
			   + "    }\n"
			   + "  ],\n"
			   + "  \"register_sale_payments\": [\n"
			   + "    {\n"
			   + "      \"id\": \"fa7d04ba-c21e-855e-11e8-25c0fd57804a\",\n"
			   + "      \"payment_date\": \"2018-03-12T06:45:44Z\",\n"
			   + "      \"amount\": \"339.8\",\n"
			   + "      \"retailer_payment_type_id\": \"0afa8de1-1450-11e8-edec-056e6ed01696\",\n"
			   + "      \"payment_type_id\": 1,\n"
			   + "      \"retailer_payment_type\": {\n"
			   + "        \"id\": \"0afa8de1-1450-11e8-edec-056e6ed01696\",\n"
			   + "        \"name\": \"Cash\",\n"
			   + "        \"payment_type_id\": \"1\",\n"
			   + "        \"config\": \"{\\\"rounding\\\":\\\"0.10\\\",\\\"algorithm\\\":\\\"round-mid-down\\\"}\"\n"
			   + "      },\n"
			   + "      \"payment_type\": {\n"
			   + "        \"id\": \"1\",\n"
			   + "        \"name\": \"Cash\",\n"
			   + "        \"has_native_support\": false\n"
			   + "      },\n"
			   + "      \"register_sale\": {\n"
			   + "        \"id\": \"fa7d04ba-c21e-855e-11e8-25c0e8cd045e\",\n"
			   + "        \"source\": \"USER\",\n"
			   + "        \"source_id\": null,\n"
			   + "        \"sale_date\": \"2018-03-12T06:45:44Z\",\n"
			   + "        \"status\": \"CLOSED\",\n"
			   + "        \"user_id\": \"0afa8de1-147c-11e8-edec-104f6535a398\",\n"
			   + "        \"customer_id\": \"0afa8de1-147c-11e8-edec-25c09e4a6f05\",\n"
			   + "        \"register_id\": \"0afa8de1-1450-11e8-edec-056e6ecf4cd9\",\n"
			   + "        \"market_id\": \"1\",\n"
			   + "        \"invoice_number\": \"1\",\n"
			   + "        \"short_code\": \"enjukv\",\n"
			   + "        \"totals\": {\n"
			   + "          \"total_price\": \"295.47826\",\n"
			   + "          \"total_loyalty\": \"0.00000\",\n"
			   + "          \"total_tax\": \"44.32174\",\n"
			   + "          \"total_payment\": \"339.80000\",\n"
			   + "          \"total_to_pay\": \"0.00000\"\n"
			   + "        },\n"
			   + "        \"note\": \"\",\n"
			   + "        \"updated_at\": \"2018-03-12T06:45:59+00:00\",\n"
			   + "        \"created_at\": \"2018-03-12 06:45:59\"\n"
			   + "      }\n"
			   + "    }\n"
			   + "  ],\n"
			   + "  \"taxes\": [\n"
			   + "    {\n"
			   + "      \"id\": \"6ecd4ad7-056e-11e8-adec-0afa8de11450\",\n"
			   + "      \"name\": \"GST\",\n"
			   + "      \"rate\": \"0.15000\",\n"
			   + "      \"tax\": 44.32174\n"
			   + "    }\n"
			   + "  ]\n"
			   + "}";

			System.out.println(json);

			Sale sale = gson.fromJson(json, Sale.class);
			salesByCustomer.put(sale.getCustomer().getId(), sale);
		}
	}

	public void save(Sale sale) {
		salesByCustomer.put(sale.getCustomer().getId(), sale);
		salesBySaleId.put(sale.getId(), sale);
	}

	public Collection<Sale> getSales(String customerId) {
		return salesByCustomer.get(customerId);
	}

	public Boolean doesSaleExist(String saleId) {
		return salesBySaleId.containsKey(saleId);
	}

	public Boolean doesCustomerExist(String customerId) {
		return salesByCustomer.containsKey(customerId);
	}

	public Summary getSummary(String customerId) {
		Collection<Sale> custSales = getSales(customerId);

		Summary summary = new Summary();
		summary.setNumberOfSales(custSales.size());
		Double totalPayment = custSales.stream().mapToDouble(sale -> sale.getTotals().getTotalPayment()).sum();
		summary.setTotalPayment(totalPayment);
		summary.setGroup(totalPayment <= THRESHHOLD ? "Regular Customers" : "VIP Customers");

		return summary;
	}

}
