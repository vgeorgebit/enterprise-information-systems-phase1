package resource;

import dao.SaleDAO;
import model.ErrorMessage;
import model.Sale;
import org.jooby.Jooby;
import org.jooby.MediaType;
import org.jooby.Status;

public class SalesResource extends Jooby {

	public SalesResource(SaleDAO dao) {

		post("/api/sales", (req, rsp) -> {

			Sale sale = req.body(Sale.class);

			if (!dao.doesSaleExist(sale.getId())) {
				dao.save(sale);
				rsp.status(Status.CREATED).send(sale);
			} else {
				rsp.status(Status.UNPROCESSABLE_ENTITY).send(new ErrorMessage("That sale already exists"));
			}

		}).produces(MediaType.json).consumes(MediaType.json);

	}

}
