swagger: '2.0'
info:
  title: Customer Accounts
  version: '1.0'
  description: REST service for managing customer accounts.
basePath: /api
host: localhost:8080
schemes:
  - http
  - https
consumes:
  - application/json
produces:
  - application/json
tags:
  - name: Customers
    description: Operations that apply to the collection of customer accounts.
  - name: Customer
    description: Operations that apply to a single customer account.
paths:
  /customers:
    get:
      summary: Get customer accounts.
      description: Get the complete details for all registered customers.
      operationId: get customers
      tags:
        - Customers
      responses:
        '200':
          description: OK.
          schema:
            type: array
            items:
              $ref: '#/definitions/Customer'
    post:
      summary: Add a new customer account.
      description: Add a new customer account.
      operationId: create customer
      tags:
        - Customers
      responses:
        '201':
          description: Created.
          schema:
            $ref: '#/definitions/Customer'
        '422':
          description: <b>Not created.</b> Customer with that username already exists.
          schema:
            $ref: '#/definitions/Error'
      parameters:
        - name: Customer
          in: body
          required: true
          schema:
            $ref: '#/definitions/Customer'
  '/customers/customer/{email}':
    parameters:
      - name: email
        description: The customer's email address.
        in: path
        required: true
        type: string
    put:
      summary: Update a customer account.
      description: Replace an existing customer account with a new version.
      operationId: update customer
      tags:
        - Customer
      responses:
        '200':
          description: Updated.
          schema:
            $ref: '#/definitions/Customer'
        '404':
          description: <b>Not updated.</b> Customer not found.
          schema:
            $ref: '#/definitions/Error'
        '409':
          description: <b>Not updated.</b> Changing a customer's email address is not permitted via this operation.
          schema:
            $ref: '#/definitions/Error'
      parameters:
        - name: customer
          in: body
          required: true
          schema:
            $ref: '#/definitions/Customer'
    delete:
      summary: Delete a customer account.
      description: Delete an existing customer account.
      operationId: delete customer
      tags:
        - Customer
      responses:
        '204':
          description: Deleted.email
        '404':
          description: <b>Not deleted.</b> Customer not found.
          schema:
            $ref: '#/definitions/Error'
definitions:
  Customer:
    type: object
    properties:
      username:
        type: string
      firstName:
        type: string
      lastName:
        type: string
      email:
        type: string
        description: Unique.  Immutable.
      group:
        type: string
      uri:
        type: string
        description: Server generated.
    required:
      - username
      - firstName
      - lastName
      - email
      - group
  Error:
    type: object
    properties:
      reason:
        type: string
        description: A message describing the reason for the error.
    required:
      - reason
