
import api.CustomerApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import api.CustomersApi;
import domain.Customer;
import java.io.IOException;
import java.util.List;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CustomersTest {

	Retrofit retrofit = new Retrofit.Builder()
	   .baseUrl("http://localhost:8080/api/")
	   .addConverterFactory(GsonConverterFactory.create())
	   .build();

	CustomersApi customers = retrofit.create(CustomersApi.class);
	CustomerApi customer = retrofit.create(CustomerApi.class);

	Customer cust1;
	Customer cust2;
	Customer cust3;

	@Before
	public void setUp() throws IOException {
		cust1 = new Customer()
		   .username("cust1_username")
		   .firstName("cust1_firstName")
		   .lastName("cust1_lastName")
		   .email("cust1@example.net")
		   .uri("/api/customers/customer/cust1_username")
		   .group("regular");

		cust2 = new Customer()
		   .username("cust2_username")
		   .firstName("cust2_firstName")
		   .lastName("cust2_lastName")
		   .email("cust2@example.net")
		   .uri("/api/customers/customer/cust2_username")
		   .group("regular");

		cust3 = new Customer()
		   .username("cust3_username")
		   .firstName("cust3_firstName")
		   .lastName("cust3_lastName")
		   .email("cust3@example.net")
		   .uri("/api/customers/customer/cust3_username")
		   .group("regular");

		customers.createCustomer(cust1).execute();
		customers.createCustomer(cust2).execute();
	}

	@After
	public void cleanUp() throws IOException {
		customer.deleteCustomer(cust1.getEmail()).execute();
		customer.deleteCustomer(cust2.getEmail()).execute();
		customer.deleteCustomer(cust3.getEmail()).execute();
	}

	@Test
	public void testCreateAccount() throws IOException {
		customers.createCustomer(cust3).execute().body();
		List<Customer> customersResponse = customers.getCustomers().execute().body();
		assertThat(customersResponse, hasItem(cust3));
	}

	@Test
	public void testGetAllAccounts() throws IOException {
		List<Customer> customersResponse = customers.getCustomers().execute().body();
		assertThat(customersResponse, hasItems(cust1, cust2));
	}

	@Test
	public void testUpdateAccount() throws IOException {
		cust1.setFirstName("new firstname");
		cust1.setGroup("VIP");
		customer.updateCustomer(cust1, cust1.getEmail()).execute();

		List<Customer> customersResponse = customers.getCustomers().execute().body();

		@SuppressWarnings("null")
		Customer updatedCust1 = customersResponse.get(customersResponse.indexOf(cust1));

		assertThat(updatedCust1.getFirstName(), is("new firstname"));
		assertThat(updatedCust1.getGroup(), is("VIP"));
	}

}
