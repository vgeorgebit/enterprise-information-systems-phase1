
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import domain.Customer;
import domain.Sale;
import domain.SaleItem;
import api.SalesApi;
import api.SalesForCustomerApi;
import domain.Summary;
import domain.Totals;
import java.io.IOException;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import java.math.BigDecimal;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SalesTest {

	Retrofit retrofit = new Retrofit.Builder()
	   .baseUrl("http://localhost:8081/api/")
	   .addConverterFactory(GsonConverterFactory.create())
	   .build();

	SalesApi sales = retrofit.create(SalesApi.class);
	SalesForCustomerApi customers = retrofit.create(SalesForCustomerApi.class);

	Customer cust1;
	Sale sale1;
	Sale sale2;

	@Before
	public void setUp() throws IOException {
		cust1 = new Customer()
		   .id("cust1_id")
		   .firstName("cust1_firstName")
		   .lastName("cust1_lastName")
		   .email("cust1@example.net")
		   .customerGroupId("regular");

		sale1 = new Sale()
		   .id("1")
		   .saleDate("today")
		   .customer(cust1)
		   .addRegisterSaleProductsItem(new SaleItem().price(100.00).productId("12345").quantity(2.0))
		   .totals(new Totals().totalPayment(200.00).totalPrice(200.00).totalTax(0.0));

		sale2 = new Sale()
		   .id("2")
		   .saleDate("today")
		   .customer(cust1)
		   .addRegisterSaleProductsItem(new SaleItem().price(5000.00).productId("54321").quantity(2.0))
		   .totals(new Totals().totalPayment(10000.00).totalPrice(10000.00).totalTax(0.0));

		sales.addNewSale(sale1).execute();
		sales.addNewSale(sale2).execute();
	}

	@Test
	public void testCreateAndGetSales() throws IOException {
		List<Sale> salesResponse = customers.getCustomerSales(cust1.getId()).execute().body();
		assertThat(salesResponse, hasItems(sale1, sale2));
	}

	@Test
	@SuppressWarnings("null")
	public void testGetSummary() throws IOException {
		Summary summary = customers.getCustomerSummary(cust1.getId()).execute().body();
		assertThat(summary.getNumberOfSales(), is(2));
		assertThat(summary.getTotalPayment(), is(new BigDecimal("10200.0")));
		assertThat(summary.getGroup(), is("VIP Customers"));
	}

}
